package com.accenture.dao;

import java.util.List;

import com.accenture.model.Search;

public interface SearchDAO<T> {
	public List<Search> searchLike(String name);
}
