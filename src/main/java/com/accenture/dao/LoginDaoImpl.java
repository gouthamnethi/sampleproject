package com.accenture.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.accenture.model.Login;
import com.accenture.model.Search;

public class LoginDaoImpl implements LoginDao<Login>,SearchDAO<Search> {
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public boolean login(String username,String password) throws IncorrectResultSizeDataAccessException{
		boolean userFound=true;
		Login lo;
		try {
		String sql = "SELECT * FROM login WHERE username=? AND password=?";
		lo = jdbcTemplate.queryForObject(sql, new LoginMapper(),username,password);
		}
		catch(DataAccessException e) {
			lo=null;
		}
		if(lo==null) {
			userFound=false;
		}
		return userFound;
	}
	
	@Override
	public List<Search> searchLike(String name) {
		// TODO Auto-generated method stub
		String sql= "SELECT * FROM search WHERE name LIKE ?";
		//Search list = jdbcTemplate.queryForObject(sql, new SearchMapper(),name);
		return  jdbcTemplate.query(sql, new SearchMapper(),name);
	}
	
	class LoginMapper implements RowMapper<Login>{

		@Override
		public Login mapRow(ResultSet arg0, int arg1) throws SQLException {
			// TODO Auto-generated method stub
			Login login = new Login();
			login.setUsername(arg0.getString("username"));
			login.setPassword(arg0.getString("password"));
			return login;
		}
		
	}
	
	class SearchMapper implements RowMapper<Search>{

		@Override
		public Search mapRow(ResultSet arg0, int arg1) throws SQLException {
			// TODO Auto-generated method stub
			Search search = new Search();
			search.setId(arg0.getInt("id"));
			search.setName(arg0.getString("name"));
			search.setRole(arg0.getString("role"));
			return search;
		}
		
	}



}
