package com.accenture.dao;

public interface LoginDao<T> {
	public boolean login(String username,String password);

}
