package com.accenture.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.accenture.dao.LoginDao;
import com.accenture.dao.SearchDAO;
import com.accenture.model.Login;
import com.accenture.model.Search;

public class LoginServiceImpl implements LoginService<Login> {

	@Autowired
	LoginDao<Login> loginDao;
	
	@Autowired
	SearchDAO<Search> searchDAO;

	public LoginServiceImpl() {
		super();
	}

	public LoginServiceImpl(LoginDao<Login> loginDao,SearchDAO<Search> searchDAO) {
		super();
		this.loginDao = loginDao;
		this.searchDAO = searchDAO;
	}

	@Override
	public boolean login(String username, String password) {
		// TODO Auto-generated method stub
		return loginDao.login(username, password);
	}

	@Override
	public List<Search> searchLike(String name) {
		// TODO Auto-generated method stub
		return searchDAO.searchLike(name);
	}
	
	

}
