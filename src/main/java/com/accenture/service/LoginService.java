package com.accenture.service;

import java.util.List;

import com.accenture.model.Search;

public interface LoginService<T> {
	public boolean login(String username,String password);
	
	public List<Search> searchLike(String name);
}
