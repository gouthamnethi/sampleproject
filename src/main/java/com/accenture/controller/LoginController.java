package com.accenture.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.accenture.model.Login;
import com.accenture.model.Search;
import com.accenture.service.LoginService;

@Controller
public class LoginController {
	@Autowired
	LoginService<Login> loginService;

	@RequestMapping(value = "/")
	public String home(Model model) {
		System.out.println("here");
		model.addAttribute("message", "Added an attribute value");
		return "Home";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login() {
		ModelAndView mv = new ModelAndView("Login", "Training", new Login());
		return mv;
	}

	@RequestMapping(value = "/logging", method = RequestMethod.POST)
	public ModelAndView Success(@ModelAttribute("Training") Login training) throws Exception {
		ModelAndView mv = new ModelAndView();
			boolean userFound = false;
			userFound=loginService.login(training.getUsername(), training.getPassword());
			if(userFound==true) {
				mv.addObject("searchObj", new Search());
				mv.setViewName("Search");
			}
			else {
				mv.addObject("messa","Sorry! Incorrect User Name or Password");
				mv.setViewName("Failure");
			}
		return mv;
	}
	
	@RequestMapping(value="/SearchResult",method=RequestMethod.POST)
	public ModelAndView searching(Model model,@ModelAttribute("searchObj") Search search) throws Exception{
		if(search.getName().equals(null)) {
			return new ModelAndView("EmptySearch","emp","No Keyword Entered");
		}
		else {
//		String name = "%";
//		name=name+search.getName()+"%";
		String name=search.getName();
		ModelAndView mv = new ModelAndView("ResultSearch","Res",loginService.searchLike(name));
		return mv;
		}
		
			
	}
	
	@RequestMapping(value="/Home", method=RequestMethod.GET)
	public ModelAndView homePage() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("Home");
		return mv;
	}
	
	@ExceptionHandler(value=Exception.class)
	public ModelAndView handleAllExceptions(Exception exception) {
		ModelAndView m=new ModelAndView();
		m.addObject("exception",exception.getMessage());
		m.setViewName("GeneralizedExceptionHandlerPage");
		return m;
	}
}
