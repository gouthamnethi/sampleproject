package com.accenture.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.accenture.dao.LoginDao;
import com.accenture.dao.LoginDaoImpl;
import com.accenture.dao.SearchDAO;
import com.accenture.model.Login;
import com.accenture.model.Search;
import com.accenture.service.LoginService;
import com.accenture.service.LoginServiceImpl;

public class AppConfig {
	@Bean
	public DataSource mySqlDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/project");
		dataSource.setUsername("root");
		dataSource.setPassword("root");

		return dataSource;
	}

	@Bean
	public JdbcTemplate jdbcTemplate() {
		return new JdbcTemplate(mySqlDataSource());

	}

	@Bean
	public LoginDao<Login> loginDao() {
		return new LoginDaoImpl();
	}
	
	@Bean
	public SearchDAO<Search> searchDAO(){
		return new LoginDaoImpl();
	}
	
	@Bean("loginService")
	public LoginService<Login> loginService() {
		return new LoginServiceImpl(loginDao(),searchDAO());
	}

}
