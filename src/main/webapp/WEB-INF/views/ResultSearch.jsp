<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<center>
<h2>Search Result</h2>
<br><br>
<c:if test="${not empty Res}">
<table border="2">
<caption>Results are</caption>
<tr>
<th>Id</th>
<th>Name</th>
<th>Role</th>
</tr>
<c:forEach var="i" items="${Res}">
<tr>
<td><c:out value="${i.id}"/></td>
<td><c:out value="${i.name}"/></td>
<td><c:out value="${i.role}"/></td>
</tr>
</c:forEach>
</table>
</c:if>
<c:if test="${empty Res}">
<h3>No data Found.</h3>
</c:if>
<br><br>
<a href="Home">Home</a>
</center>
</body>
</html>