<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<center>
<h2>Search Page</h2>
<br><br>
<form:form modelAttribute="searchObj" action="SearchResult" method="POST">
<table border="2">
<tr>
<th>Enter the name to be searched</th>
<td><form:input path="name"/></td>
</tr>
</table>
<br><br>
<input type="submit" value="Search"/>
</form:form>
<br><br>
<a href="Home">Home</a>
</center>
</body>
</html>