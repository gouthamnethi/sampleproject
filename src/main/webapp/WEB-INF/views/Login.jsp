<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login Page</title>
</head>
<body>
<center>
<h2>Login Form</h2>
<br><br>

<form:form modelAttribute="Training" action="logging" method="post">
<table border='2'>
<tr>
<th>Enter your User Name:</th>
<td><form:input path="username"/>
</td>
</tr>
<tr>
<th>Enter the Password:</th>
<td><form:input path="password" type="password"/></td>
</tr>
</table>
<br><br>
<input type="submit" value="Login"/>
</form:form>
<br><br>
<a href="Home">Home</a>
</center>
</body>
</html>